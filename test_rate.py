from argparse import ArgumentParser
import rate_sim


def generate_hr(mean_hr_per_min=40, k=100):
    g = rate_sim.rate_per_second(events_per_min=mean_hr_per_min)
    for i in range(k):
        time, rate = next(g)
        print(f"At time={time} HR={rate}")


if __name__ == '__main__':
    parser = ArgumentParser(prog="test_rate", description="""Simulate HR""")
    parser.add_argument('-k', type=int, default=100,
                        help="How many rate values to print")
    parser.add_argument("events_per_min", nargs='?', metavar='EVENTS_PER_MIN',
                        default=40, help="Mean beats per minute")
    args = parser.parse_args()
    generate_hr(int(args.events_per_min), int(args.k))
