import asyncio
import concurrent.futures
import time

import ulid
import redis

import rate_sim
import temperature_sim
import sleep_sim

def add_to_stream(r, stream, v, maxlen=None):
    """Add to the given measurement value `v` to the Redis `stream`"""
    uid = ulid.new()
    tm = uid.timestamp().int
    d = {b't': str(tm).encode("UTF-8"), b'ulid': uid.str.encode("UTF-8"), b'v': v.encode("UTF-8")}
    r.xadd(stream, d, maxlen=maxlen)
    return uid.str

async def breathrate_coro(r, mean_rate):
    g = rate_sim.rate_per_second(mean_rate)
    stream = "sensors.breathing"
    for _, rate in g:
        uid = add_to_stream(r, stream, str(rate))
        print("{}: Breathing rate: {}".format(uid, rate))
        await asyncio.sleep(1)

async def heartrate_coro(r, mean_rate):
    g = rate_sim.rate_per_second(mean_rate)
    stream = "sensors.heartrate"
    for _, rate in g:
        uid = add_to_stream(r, stream, str(rate))
        print("{}: Heartrate: {}".format(uid, rate))
        await asyncio.sleep(1)

async def temperature_coro(r):
    g = temperature_sim.temperature_simulator_seconds()
    stream = "sensors.thermometer"
    for _, t in g:
        uid = add_to_stream(r, stream, str(t))
        print("{}: Temperature: {}".format(uid, t))
        await asyncio.sleep(1)

async def sleep_coro(r):
    g = sleep_sim.sleep_simulator()
    stream = "sensors.sleeping"
    _, stage, duration, _ = g.__next__()
    prev_stage = stage
    for _, stage, duration, _ in g:
        uid = add_to_stream(r, stream, str(sleep_sim.stage_id(prev_stage)))
        print("{}: Sleep: {}".format(uid, prev_stage))
        prev_stage = stage
        if duration > 0:
            print("about to sleep for {}".format(duration))
            await asyncio.sleep(duration)


async def sleep_process():
    loop = asyncio.get_running_loop()
    # Run in a custom process pool:
    with concurrent.futures.ProcessPoolExecutor(max_workers=1) as pool:
        await loop.run_in_executor(pool, sleep_coro)


async def main_loop(r, mean_breathing_rate=40, mean_heartrate=150):
    await asyncio.gather(
        breathrate_coro(r, mean_breathing_rate),
        heartrate_coro(r, mean_heartrate),
        temperature_coro(r),
        sleep_coro(r))

if __name__ == '__main__':
    r = redis.Redis()
    asyncio.run(main_loop(r))