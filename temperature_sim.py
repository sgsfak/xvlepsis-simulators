import random
import math


def temperature(time, max=39.5, min=36.5, s=30, period=180, rand=None):
    c = 2 * s**2
    a = max-min
    x = time % period
    y = a*math.exp(-x**2/c) + min
    if rand:
        y += 0.5 - rand.random()
    return int(y*100)/100.0


def temperature_simulator(seed=0xDEAD, fever_hours=4):
    """Generate a measurement for temperature ranging between 36 and 40.
    The returned values are tuples of time and temperature, where time
    supposedly is in minutes. Fever happens every 4 hours by default"""
    r = random.Random(seed)
    period = fever_hours * 60
    current_time = 0
    while True:
        current_time += 1
        t = temperature(current_time, period=period, rand=r)
        yield (current_time, t)


def temperature_simulator_seconds(seed=0xDEAD, fever_hours=4):
    """Generate a measurement for temperature ranging between 36 and 40.
    The returned values are tuples of time and temperature, where time
    supposedly is in seconds"""
    r = random.Random(0xCAFE)
    for t, v in temperature_simulator(seed, fever_hours):
        m = (t - 1)*60
        for i in range(1, 60):
            vv = 0.1 - r.random()/5
            yield (m + i, int(100.0* (v+vv)) / 100.0)
        yield (t*60, v)

# from itertools import islice
# from pprint import pprint

# pprint(list(islice(temperature_simulator(), 2)))
# pprint(list(islice(temperature_simulator_seconds(), 120)))
