import random

# AWAKE = 0
# LIGHT = 1
# DEEP = 2
# REM = 3

stages = ['AWAKE', 'LIGHT', 'DEEP', 'REM']
stages_ids = {s: i for i, s in enumerate(stages)}

def stage_id(stage):
    return stages_ids[stage]

def setup():
    """This function builds a stochastic matrix for the transitions
    between the different "sleep stages"..

    You can use the function `probmat_to_dot` to have the matrix
    in the graphviz "dot" format, see the permalink of this:
    https://rise4fun.com/Agl/7Lba
    """
    P = []
    for i, _ in enumerate(stages):
        P.append([])
        for j, _ in enumerate(stages):
            P[i].append(0)
    P[stages_ids['AWAKE']][stages_ids['AWAKE']] = 0.7
    P[stages_ids['AWAKE']][stages_ids['LIGHT']] = 0.3

    P[stages_ids['LIGHT']][stages_ids['LIGHT']] = 0.2
    P[stages_ids['LIGHT']][stages_ids['AWAKE']] = 0.3
    P[stages_ids['LIGHT']][stages_ids['DEEP']] = 0.5

    P[stages_ids['DEEP']][stages_ids['DEEP']] = 0.3
    P[stages_ids['DEEP']][stages_ids['REM']] = 0.5
    P[stages_ids['DEEP']][stages_ids['LIGHT']] = 0.2

    P[stages_ids['REM']][stages_ids['REM']] = 0.2
    P[stages_ids['REM']][stages_ids['DEEP']] = 0.1
    P[stages_ids['REM']][stages_ids['LIGHT']] = 0.7
    return P

def probmat_to_dot(P, names):
    s = "digraph g {"
    s += "\nrankdir=\"LR\""
    for i,n in enumerate(names):
        s += "\na{} [label=\"{}\"]".format(i, n)
    for i in range(len(P)):
        for j in range(len(P[i])):
            if P[i][j] > 0:
                s += "\na{} -> a{} [label=\"{}\"]".format(i, j, P[i][j])
    s += "\n}"
    return s


def sleep_simulator(period=180, max_sleep_time=3600, seed=0xDEAD):
    """Simulates the generation of "sleep stage events". A stage is one of
     'AWAKE', 'LIGHT', 'DEEP', and 'REM' and an event is a transition to a
     different stage from the current one. Each event is a tuple containing
     the following information:

     - `current_time` is the time in secs of transition. It starts from 0
     - `current_stage` is the new stage that we transition into
     - `stage_duration` is the duration of the _previous_ stage
     - `sleeping_time` is the accumulated time between the AWAKE stages

     The first event generated will be always (0, 'AWAKE', 0, 0). Please
     also note that this is a infinite generator, if you want a finite number
     of events you can use `iterools.islice` e.g.
              list(islice(sleep_simulator(), 10))

    For the probabilities given in the `setup()` function, a simulation provides:

    >>> Counter(s for _, s, _, _ in islice(sleep_simulator(), 1000000))
    Counter({'LIGHT': 391748, 'DEEP': 265224, 'REM': 187209, 'AWAKE': 155819})

    i.e. there are 391748 transitions into 'LIGHT' stage, 265224 into 'DEEP', etc.
    Please note that the number of transitions is not indicative of the time spent
    in each stage. For this, check the `test()` function below.
    """
    P = setup()
    r = random.Random(seed)
    stage_duration = 0
    current_state = 'AWAKE'
    sleep_start = 0
    current_time = 0
    sleeping_time = 0
    yield current_time, current_state, stage_duration, sleeping_time
    while True:
        # Period is the time in (virtual) seconds between state transitions
        stage_duration += period
        current_time += period
        # Force wakeup is she is sleeping more than 1 hour or whatever the
        # given max_sleep_time is:
        if current_state is not 'AWAKE' and sleeping_time > max_sleep_time:
            next_state = 'AWAKE'
        else:
            next_state = r.choices(stages, P[stages_ids[current_state]])[0]
        if next_state is not current_state:
            if current_state is not 'AWAKE':
                sleeping_time = current_time - sleep_start
            else:
                sleeping_time = 0
            if current_state is 'AWAKE':
                # was AWAKE and now starts sleeping
                sleep_start = current_time
            elif next_state is 'AWAKE':
                # was sleeping and now woke up
                sleep_start = 0
            current_state = next_state
            yield current_time, current_state, stage_duration, sleeping_time
            stage_duration = 0


def test(n=1000, seed=0xCAFFE):
    """Computes the percentage of time spent in each sleep stage performing
    n simulations. Example:

    >>> test(100000)
    {'AWAKE': 32.34, 'LIGHT': 30.08, 'DEEP': 23.16, 'REM': 14.40}
    """
    from itertools import islice
    from collections import Counter
    g0 = (s for _, s, _, _ in islice(sleep_simulator(seed=seed), 0, n))
    g1 = (t for _, _, t, _ in islice(sleep_simulator(seed=seed), 1, n))
    c = Counter()
    for s, t in zip(g0, g1):
        c[s] += t
    # return c
    tot = sum(c.values())
    return dict((s, t*100/tot) for s,t in c.items())


