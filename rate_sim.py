import random


def arrivals(events_per_min=40, seed=0xDEAD):
    # On average every minute (60 seconds) we have events_per_min arrivals
    # So we have one event (arrival) every 60/events_per_min seconds
    rate = events_per_min / 60.0
    r = random.Random(seed)
    while True:
        #yield -math.log(1.0 - r.random()) / rate
        yield r.expovariate(rate)


def arrivals_simulator(events_per_min=40, seed=0xDEAD):
    current_time = 0
    for t in arrivals(events_per_min, seed):
        next_time = current_time + t
        current_time = next_time
        yield current_time

def arrivals_simulator_upto(end, events_per_min=40, seed=0xDEAD):
    for t in arrivals_simulator(events_per_min, seed=seed):
        if t > end:
            return
        yield t


def rate_per_second(events_per_min=40, seed=0xDEAD):
    from collections import deque
    increase_seconds = 1
    g = arrivals_simulator(events_per_min, seed=seed)
    window = deque()
    for t in g:
        window.append(t)
        if t > 60:
            break
    current_time = 60
    yield (current_time, len(window))
    for t in g:
        bound = t - 60
        while current_time < bound:
            while window[0] < current_time:
                window.popleft()
            current_time += increase_seconds
            yield (current_time, len(window))
        window.append(t)


#from pprint import pprint
#from itertools import islice
#pprint(list(islice(arrivals(), 120)))
#pprint(list(islice(arrivals_simulator(), 120)))
#pprint(list(islice(rate_per_second(), 120)))


