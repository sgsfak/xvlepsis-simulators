## xVlepsis Simulators

This is some Python code for generating some fake measurement values for newborns babies. The following types of simulators have been implemented:

* `temperature`: The temperature values produced follow a "bell-shaped" function with the peak value of (around) 40 degrees
  
* `breathing rate`: For a mean breathing rate of 40 breaths per minute, we create random intervals between the breaths (using a exponential distribution) and then we measure how many we have every minute
  
* `heart rate`: The same approach with the breathing rate, mean heart rate of 150

* `sleep`: A state transition probability matrix is followed between the Awake, Light, Deep, and REM stages of sleep. The following image is a visual representation of the stage transitions and their probabilities:

<img width="300" src="sleep_stage_transitions.png">
   
   